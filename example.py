import flipper_engine
import time
import threading

flipper = flipper_engine.Flipper_Engine()

def loop():
    flipper = flipper_engine.Flipper_Engine()
    while True:
        #val = flipper.set_item_state('hs.flipper.ItemController1', 'button_left', 2)
        flipper.item_proxy['button_left'] = 0
        print(f"button state changed: {flipper.item_proxy['button_left']}")
        time.sleep(2)
        #val = flipper.set_item_state('hs.flipper.ItemController1', 'button_left', 1)
        flipper.item_proxy.button_left = 1
        print(f"button state changed: {flipper.item_proxy['button_left']}")
        time.sleep(2)
        flipper.item_proxy.button_right = 0
        print(f"button state changed: {flipper.item_proxy['button_right']}")
        time.sleep(2)
        flipper.item_proxy.button_right = 1
        print(f"button state changed: {flipper.item_proxy['button_right']}")
        time.sleep(2)

thread = threading.Thread(target=loop)
thread.start()
        

@flipper.on_item_state_changed('button_left')
def button_handler(item_id: str, state: int):
    # set flipper left to active (1) when state of left button is set to (1)
    print(f"state of {item_id} has been changed: new state = {state}")
    # flipper.set_item_state('flipper_left', 1)
    # print(f"state of flipper right set to {flipper.get_item_state('flipper_left')}")
    time.sleep(1)

@flipper.on_item_state_changed('button_right')
def button_handler(item_id: str, state: int):
    # set flipper left to active (1) when state of left button is set to (1)
    print(f"state of {item_id} has been changed: new state = {state}")
    # flipper.set_item_state('flipper_left', 1)
    # print(f"state of flipper right set to {flipper.get_item_state('flipper_left')}")
    time.sleep(1)

# while True:
#     #flipper.set_item_state('hs.flipper.ItemController1', 'flipper_right', 2)
#     print(flipper.get_item_state('hs.flipper.ItemController1', 'flipper_right'))
#     time.sleep(2)
    
