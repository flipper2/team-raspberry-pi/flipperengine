# Flipper Engine API

Flipper Engine API is a simple communication interface between client and the CANOpen satellites, with tools that allow users to easily obtain or set states of items that are registered to the satallites and more.

# Example

In the following example, the callback function "button_handler" is subscribed to a signal from the satellite indicating a change in item state of a particular item. In this example, button_handler will be called if the incoming signal states that the state of an item named 'button_left' has been changed to 1.
```py
import flipper_engine
import time

flipper = flipper_engine.Flipper_Engine()

@flipper.on_item_state_changed('button_left', 1)
def button_handler(item_id: str, state: int):
    # set flipper left to active (1) when state of left button is set to (1)
    print(f"state of {item_id} has been changed: new state = {state}")
    flipper.set_item_state('flipper_left', 1)
    print(f"state of flipper right set to {flipper.get_item_state('flipper_left')}")
    time.sleep(1)
    
```
To run this example yourself, make sure to first run the dummy server "signal_periodic.py" with
```
$ python3 signal_periodic.py
```
