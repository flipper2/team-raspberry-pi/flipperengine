import dbus
from dbus.mainloop.glib import DBusGMainLoop
import threading
from gi.repository import GLib
import functools

#DBusGMainLoop(set_as_default=True)

class Flipper_Engine():
    '''
    The Flipper_Engine class allows communication between client and CANOpen translator via DBUS.

    Attributes:
        ItemProxy (class): proxy class for Item

        get_item_state (proxy_method): retrieve state of specified item.

        set_item_state (proxy_method): set state of specified item.

        item_proxy (object): instance of ItemProxy class

        on_item_state_changed (decorator): triggers callback function when state of specified item has been changed

        item_follow (method): on source item state changed: update state(s) of selected item(s). ie. button right pressed -> flipper right set 

        item_award

    Examples:
        # get state of Item 'flipper_left'
        >>> engine.get_item_state('flipper_left')

        # set state of Item 'flipper_left' to 0
        >>> engine.set_item_state('flipper_left', 0)

        # get Item state directly using item_proxy 
        >>> engine.item_proxy['flipper_left'] 
        # or
        >>> engine.item_proxy.flipper_left

        # set Item state directly using item_proxy
        >>> engine.item_proxy['flipper_left']  = 0 
        # or
        >>> engine.item_proxy.flipper_left = 0

        # using decorator on a callback function
        >>> @flipper.on_item_state_changed('button_left', 1)
        >>> def button_handler(item_id: str, state: int):
        >>>     print(f"state of {item_id} has been changed: new state = {state}")

    '''
    class ItemProxy(object):
        '''
        ItemProxy (class): proxy class for Item
        '''
        def __init__(self, engine) -> None:
            # circumvent the __setattr__ callback
            self.__dict__['engine'] = engine

        def __setattr__(self, name: str, value: int) -> int:
            return self.engine.set_item_state(name, value)
        
        def __setitem__(self, name: str, value: int) -> int:
            return self.__setattr__(name, value)
        
        def __getattr__(self, name: str) -> int:
            return self.engine.get_item_state(name)
        
        def __getitem__(self, name: str) -> int:
            return self.__getattr__(name)

    def __init__(self):
        DBusGMainLoop(set_as_default=True)

        self.bus = dbus.SessionBus()

        self.current_player = 1

        item_controller_proxy = self.bus.get_object('tha.flipper.ItemRegistry1', '/tha/flipper/ItemRegistry1', follow_name_owner_changes=True)

        # get-/set- method for item state
        self.get_item_state = functools.partial(
            item_controller_proxy.get_dbus_method(member='Get', dbus_interface='org.freedesktop.DBus.Properties'),
            'tha.flipper.ItemRegistry1'
        )
        self.set_item_state = functools.partial(
            item_controller_proxy.get_dbus_method(member='Set', dbus_interface='org.freedesktop.DBus.Properties'),
            'tha.flipper.ItemRegistry1'
        )
        self.get_all_items = functools.partial(
            item_controller_proxy.get_dbus_method(member='GetAll', dbus_interface='org.freedesktop.DBus.Properties'),
            'tha.flipper.ItemRegistry1'
        )

        # instantiate proxy object
        self.item_proxy = self.ItemProxy(self)

        self.points_awarded_callbacks = []

    def loop_run(self):
        GLib.MainLoop().run()

    def on_item_state_changed(self, target_item_id: str, target_state=None):
        def decorator(callback_func):
            def handler(interface: str, changes: dict, invalidated: list):
                for item_id, state in changes.items():
                    if item_id != target_item_id:
                        # in coming signal is not addressing item
                        continue 
                    if target_state is not None and target_state != state:
                        return
                
                    # user is not checking for specific state
                    # or user is checking for updates for a specific state
                    callback_func(item_id, state)
                
            self.bus.add_signal_receiver(
                handler_function=handler, 
                path='/tha/flipper/ItemRegistry1', 
                dbus_interface='org.freedesktop.DBus.Properties', 
                signal_name='PropertiesChanged'
            )
            return callback_func
        return decorator
    
    # item source = source of signal ie. a button 
    # items_target = list of target flipper components (right flipper, left flipper) that signal is addressing to
    def item_follow(self, *items_target, item_source):
        @self.on_item_state_changed(item_source)
        def callback(item_id, item_state):
            for target in items_target:
                self.set_item_state(target, item_state)

    def item_award(self, *awarding_items, awarding_amount=10):
        # for each awarding item create a callback that updates target counter
        for item in awarding_items:
            @self.on_item_state_changed(item, 1)
            def callback(item_id, item_state):
                target_counter = f'counter_player_{self.current_player}'
                current_amount = self.item_proxy[target_counter]
                current_amount += awarding_amount
                self.item_proxy[target_counter] = current_amount
                for callback in self.points_awarded_callbacks:
                    callback(self.current_player, awarding_amount, current_amount)

    def on_points_awarded(self):
        def decorator(func):
            self.points_awarded_callbacks.append(func)
            return func
        return decorator

    def set_current_player(self, player):
        self.current_player = player
    
    
